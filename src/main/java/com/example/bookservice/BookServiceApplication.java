package com.example.bookservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.*;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RequestMapping("/books")
public class BookServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookServiceApplication.class, args);
	}


	private List<Book> bookList = new ArrayList<>(Arrays.asList(
			new Book(1L, "Book 1", "Author 1"),
			new Book(2L, "Book 2", "Author 2")
	));

	@GetMapping("")
	public List<Book> findAllBooks(){
		return bookList;
	}

	@GetMapping("/{bookId}")
	public Book findBook(@PathVariable Long bookId){
		return bookList.stream().filter(book -> book.getId().equals(bookId)).findFirst().orElse(null);
	}

	@GetMapping("/entity/{bookId}")
	public ResponseEntity<Book> getBook(@PathVariable Long bookId){
        Optional<Book> book = bookList.stream().filter(b -> b.getId().equals(bookId)).findFirst();
        return ResponseEntity.ok(book.get());
	}

    @PostMapping("")
	public ResponseEntity addBook(@RequestBody Book book){
	    bookList.add(book);
	    return ResponseEntity.created(URI.create("http://localhost/books/3")).body("");
    }

}
