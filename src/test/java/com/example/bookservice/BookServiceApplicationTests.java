package com.example.bookservice;

import org.junit.Test;
import org.junit.experimental.results.ResultMatchers;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.StatusResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest(BookServiceApplication.class)
public class BookServiceApplicationTests {

	@Autowired
	private MockMvc mvc;


	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetBook() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/books/1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string("{\"id\":1,\"author\":\"Author 1\",\"title\":\"Book 1\"}"));
	}

	@Test
	public void testGetBookAsResponseEntity() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/books/entity/1")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string("{\"id\":1,\"author\":\"Author 1\",\"title\":\"Book 1\"}"));
	}

	@Test
	public void testAddBook() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/books")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"id\":3,\"author\":\"Author 3\",\"title\":\"Book 3\"}"))
				.andExpect(status().isCreated())
				.andExpect(MockMvcResultMatchers.header().string("location", "http://localhost/books/3"))
				.andExpect(content().string(""));
	}

}
